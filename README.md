Simple4epub configuration file for Tex4ht
=========================================

Description
-----------

Basic **Tex4ht** configurations for EPUB3 (also planned for EPUB2 later)
conversion, using [Blitz CSS][] Framework, with PNG images or [KaTeX][] to
display math formulas. It's designed for small and medium-sized LaTeX
articles.

- At this time, by default, math formulas are converted into PNG images.


**Required LaTeX Packages**:  `etoolbox`, `environ` and `expl3`, among others.

**Main Goal**: this configuration tries to adapt **TeX4ebook** output to ensure
compatibility with KaTeX, so there is no need to modify the original
LaTeX document manually. To do this (with `katex*` options), this configuration
transforms the necessary **LaTeX** math commands into other macros compatible
with the javascript library, maybe at the cost of losing some features.


Options
-------

### Custom options

- `katex`: It uses KaTeX library to render some math elements.

**Example of use:**

    tex4ebook -a info -c simple -f epub3 -o output mydoc.tex "katex"


Known issues
------------

- I can confirm some of the bugs in the official TeX4ebook packages included
  in the latest stable Linux distributions (Debian-10 and Ubuntu-18.04/
  LinuxMint-19), all of them with too old TeX4ebook versions (before v0.2c):
  - It does not set up the *navigation* for the EPUB3 file from the TOC of the
    TeX file (fixed in [commit 41acd30][]).
  - It does not remove empty guide elements in opf files, which leads to a
    validation error(fixed in [commit c6fc81a][]).
  - Missing `utf-8` codification at XML declaration in OPF and NCX files, which
    also leads to a validation error (fixed in [commit cf3a91d][]).

  You must update to the latest version of TeX4ht (and also Make4ht) to avoid
  these errors.

- It only really works for EPUB3 for the moment (in fact, for EPUB2 it uses
  the default TeX4ht configurations)

- Neither the table of contents nor the EPUB3 navigation have included the
  bibliography link.


Other considerations
--------------------

- Not tested on Windows/MikTeX.
- Not tested for `.mobi` files, focused only on EPUB format.
- You should have installed [Tidy][] it in order to make valid EPUB file (as
  TeX4ebook complains if you have not installed it).


[KaTeX]: <https://katex.org/>
[Blitz CSS]: <http://friendsofepub.github.io/Blitz/>
[commit 41acd30]: <https://github.com/michal-h21/tex4ebook/commit/41acd30e396bd5ad2e0d37efbd37cb17265eac32>
[commit c6fc81a]: <https://github.com/michal-h21/tex4ebook/commit/c6fc81a15038164fc24c3223652de85128ff3f59>
[commit cf3a91d]: <https://github.com/michal-h21/tex4ebook/commit/cf3a91df721a4c5669673619a987a2b90fc208e7>
[Tidy]: <http://www.html-tidy.org/>
